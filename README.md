# Steps to run

1. Run ```npm install```
2. Run ```npm start```

Alternatively, visit [here](https://analog-digital-clock.web.app/) for hosted project.