import { useEffect, useState } from "react";
import classes from "./App.module.css";

import AnalogClock from "./components/analog/AnalogClock";
import DigitalClock from "./components/digital/DigitalClock";

function App() {
  const [seconds, setSeconds] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [hours, setHours] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      const date = new Date();
      setSeconds(date.getSeconds());
      setMinutes(date.getMinutes());
      setHours(date.getHours());
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <div className={classes.container}>
      <AnalogClock hours={hours} minutes={minutes} seconds={seconds} />
      <DigitalClock hours={hours} minutes={minutes} seconds={seconds} />
    </div>
  );
}

export default App;
