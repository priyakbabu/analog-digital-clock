import classes from "./DigitalClock.module.css";
import { Fragment } from "react";
import Digit from "./Digit";

const pad = (value) => {
  return String(value).padStart(2, "0");
};

const DigitalClock = (props) => {
  const { hours, minutes, seconds } = props;
  
  const data = `${pad(hours)}${pad(minutes)}${pad(seconds)}`.split("");

  return (
    <div className={classes.container}>
      {data.map((item, index) => {
        return (
          <Fragment key={index}>
            <Digit number={item} />
            {(index === 1 || index === 3) && (
              <div className={classes.separator}>:</div>
            )}
          </Fragment>
        );
      })}
    </div>
  );
};

export default DigitalClock;
