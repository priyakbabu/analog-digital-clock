import { getOpacity } from "../../utils/util";
import classes from "./Digit.module.css";

const Digit = ({ number }) => {  
  return (
    <div className={classes.container}>
      <div
        style={{ opacity: getOpacity(number, 0) }}
        className={`${classes.number} ${classes.one}`}
      />
      <div
        style={{ opacity: getOpacity(number, 1) }}
        className={`${classes.number} ${classes.two}`}
      />
      <div
        style={{ opacity: getOpacity(number, 2) }}
        className={`${classes.number} ${classes.three}`}
      />
      <div
        style={{ opacity: getOpacity(number, 3) }}
        className={`${classes.number} ${classes.four}`}
      />
      <div
        style={{ opacity: getOpacity(number, 4) }}
        className={`${classes.number} ${classes.five}`}
      />
      <div
        style={{ opacity: getOpacity(number, 5) }}
        className={`${classes.number} ${classes.six}`}
      />
      <div
        style={{ opacity: getOpacity(number, 6) }}
        className={`${classes.number} ${classes.seven}`}
      />
    </div>
  );
};

export default Digit;
