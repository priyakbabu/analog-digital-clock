import classes from "./AnalogClock.module.css";
import AnalogHand from "./AnalogHand";

const AnalogClock = (props) => {
  const { hours, minutes, seconds } = props;

  return (
    <div className={classes.clock}>
      <AnalogHand type="seconds" degree={seconds * 6} />
      <AnalogHand type="minutes" degree={minutes * 6} />
      <AnalogHand type="hours" degree={hours * 30 + minutes * 0.5} />
    </div>
  );
};

export default AnalogClock;
