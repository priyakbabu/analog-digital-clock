import classes from "./AnalogHand.module.css";

const AnalogHand = (props) => {
  const { type, degree } = props;

  const handClass =
    type === "hours" ? classes.hoursHand
      : type === "minutes" ? classes.minutesHand : classes.secondsHand;

  const style = { transform: `rotate(${degree}deg)` };
  if (degree === 0) {
    style.transition = "none";
  }

  return (
    <div style={style} className={classes["hand-container"]}>
      <div className={`${classes.hand} ${handClass}`}></div>
    </div>
  );
};

export default AnalogHand;
